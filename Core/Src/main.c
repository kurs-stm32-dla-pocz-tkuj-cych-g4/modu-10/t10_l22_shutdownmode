/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define LED_TIMER 200
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
typedef enum
{
    RESET_CAUSE_UNKNOWN = 0,
    RESET_CAUSE_LOW_POWER_RESET,
    RESET_CAUSE_WINDOW_WATCHDOG_RESET,
    RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET,
    RESET_CAUSE_SOFTWARE_RESET,
    RESET_CAUSE_POWER_ON_POWER_DOWN_RESET,
    RESET_CAUSE_EXTERNAL_RESET_PIN_RESET,
	RESET_CAUSE_OPTIONBYTES_RESET,
    RESET_CAUSE_BROWNOUT_RESET,
}ResetCause_t;

ResetCause_t ResetCause;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void SlowDownClockTo2Mhz(void);
ResetCause_t GetResetCause(void);

void UartSend(char *Message)
{
	HAL_UART_Transmit(&hlpuart1, (uint8_t*)Message, strlen(Message), 1000);
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_LPUART1_UART_Init();
  /* USER CODE BEGIN 2 */
  uint32_t TimerLED = HAL_GetTick();

    UartSend("\n\rShutdown Mode Test\n\r");

    UartSend("Disable Wakeup Pin nr 1\n\r");
    HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1_HIGH);

    // Slow Down clock frequency below 2 MHz
    SlowDownClockTo2Mhz();

    MX_LPUART1_UART_Init(); // Reinit UART for 2 MHz HCLK

    ResetCause = GetResetCause();

	switch(ResetCause)
	{
	  case RESET_CAUSE_UNKNOWN:
		  UartSend("UNKNOWN\n\r");
		  break;
	  case RESET_CAUSE_LOW_POWER_RESET:
		  UartSend("LOW_POWER_RESET\n\r");
		  break;
	  case RESET_CAUSE_WINDOW_WATCHDOG_RESET:
		  UartSend("WINDOW_WATCHDOG_RESET\n\r");
		  break;
	  case RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET:
		  UartSend("INDEPENDENT_WATCHDOG_RESET\n\r");
		  break;
	  case RESET_CAUSE_SOFTWARE_RESET:
		  UartSend("SOFTWARE_RESET\n\r");
		  break;
	  case RESET_CAUSE_POWER_ON_POWER_DOWN_RESET:
		  UartSend("POWER-ON_RESET (POR) / POWER-DOWN_RESET (PDR)\n\r");
		  break;
	  case RESET_CAUSE_EXTERNAL_RESET_PIN_RESET:
		  UartSend("EXTERNAL_RESET_PIN_RESET\n\r");
		  break;
	  case RESET_CAUSE_BROWNOUT_RESET:
		  UartSend("BROWNOUT_RESET (BOR)\n\r");
		  break;
	  case RESET_CAUSE_OPTIONBYTES_RESET:
		  UartSend("OBTIONBYTES_RESET\n\r");
		  break;
	}

	if(__HAL_PWR_GET_FLAG(PWR_FLAG_WUF1) == 1)
	{
	  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
	  UartSend("Start from Wakeup\n\r");
	}

	if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) == 1)
	{
	  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
	  UartSend("Start from Standby\n\r");
	}

	if(__HAL_PWR_GET_FLAG(PWR_FLAG_WUFI) == 1)
	{
	  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUFI);
	  UartSend("Start from Wakeup on internal wakeup line\n\r");
	}

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	if((HAL_GetTick() - TimerLED) > LED_TIMER)
	{
		TimerLED = HAL_GetTick();
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	}

	if((HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)) == GPIO_PIN_SET)
	{
		while((HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)) == GPIO_PIN_SET)
		{} // Prevent unnecessary wakeup event

		UartSend("Enable Wakeup Pin nr 1\n\r");
		HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1_HIGH);

		UartSend("Enter Shutdown Mode\n\r");
		HAL_PWREx_EnterSHUTDOWNMode();

		UartSend("Never been there\n\r");
	}

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void SlowDownClockTo2Mhz(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	*/
	HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
	Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
							  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV8;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
	Error_Handler();
	}
}

ResetCause_t GetResetCause(void)
{
	ResetCause_t ResetCause;

    if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST))
    {
        ResetCause = RESET_CAUSE_LOW_POWER_RESET;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST))
    {
        ResetCause = RESET_CAUSE_WINDOW_WATCHDOG_RESET;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST))
    {
        ResetCause = RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST))
    {
        ResetCause = RESET_CAUSE_SOFTWARE_RESET; // This reset is induced by calling the ARM CMSIS `NVIC_SystemReset()` function!
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST))
    {
        ResetCause = RESET_CAUSE_EXTERNAL_RESET_PIN_RESET;
    }
    // Needs to come *after* checking the `RCC_FLAG_PORRST` flag in order to ensure first that the reset cause is
    // NOT a POR/PDR reset. See note below.
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST))
    {
        ResetCause = RESET_CAUSE_BROWNOUT_RESET;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_OBLRST))
	{
		ResetCause = RESET_CAUSE_OPTIONBYTES_RESET;
	}
    else
    {
        ResetCause = RESET_CAUSE_UNKNOWN;
    }

    // Clear all the reset flags or else they will remain set during future resets until system power is fully removed.
    __HAL_RCC_CLEAR_RESET_FLAGS();

    return ResetCause;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
